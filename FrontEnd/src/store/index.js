import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: null,
    userID: null,
  },
  getters: {
    loggedIn(state) {
      return state.token !== null;
    },
  },
  mutations: {
    setToken(state, newToken) {
      state.token = newToken;
    },
    setUserId(state, newVal) {
      state.userID = newVal;
    },
    deleteToken(state) {
      state.token = null;
    },
  },
  actions: {
    storeToken(context, newToken) {
      context.commit('setToken', newToken);
    },
    storeUserId(context, newVal) {
      context.commit('setUserId', newVal);
    },
    deleteToken(context) {
      if (context.getters.loggedIn) {
        context.commit('deleteToken');
      }
    },
  },
  modules: {
  },
});
