import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import ListeJeux from '@/views/ListeJeux.vue';
import SignUp from '@/views/SignUp.vue';
import Genres from '@/views/Genres.vue';
import DetailsJeu from '@/views/DetailsJeu.vue';
import Developpeurs from '@/views/Developpeurs.vue';
import Plateformes from '@/views/Plateformes.vue';
import Commande from '@/views/Commande.vue';
import Profil from '@/views/Profil.vue';
import JeuxGenre from '@/views/JeuxGenre.vue';
import JeuxDeveloppeur from '@/views/JeuxDeveloppeur.vue';
import JeuxPlateforme from '@/views/JeuxPlateforme.vue';
import Redirection from '@/views/Redirection.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    props: true,
  },
  {
    path: '/ListeJeux',
    name: 'ListeJeux',
    component: ListeJeux,
    props: true,
    meta: { value: 'Jeux' },
  },
  {
    path: '/ListeJeux/:Recherche?/:Genre?',
    name: 'ListeJeux',
    component: ListeJeux,
    props: true,
    meta: { value: 'Jeux' },
  },
  {
    path: '/SignUp',
    name: 'SignUp',
    component: SignUp,
    meta: { value: 'Sign Up' },
  },
  {
    path: '/Genres',
    name: 'Genres',
    component: Genres,
    meta: { value: 'Genres' },
    props: true,
  },
  {
    path: '/DetailsJeu/:id',
    name: 'DetailsJeu',
    component: DetailsJeu,
    props: true,
  },
  {
    path: '/Developpeurs',
    name: 'Developpeurs',
    component: Developpeurs,
    meta: { value: 'Developpeurs' },
    props: true,
  },
  {
    path: '/JeuxGenre/:idGenre',
    name: 'JeuxGenre',
    component: JeuxGenre,
    meta: { value: 'JeuxGenre' },
    props: true,
  },
  {
    path: '/JeuxPlateforme/:idPlateforme',
    name: 'JeuxPlateforme',
    component: JeuxPlateforme,
    props: true,
  },
  {
    path: '/JeuxDeveloppeur/:idDeveloppeur',
    name: 'JeuxDeveloppeur',
    component: JeuxDeveloppeur,
    props: true,
  },
  {
    path: '/Plateformes',
    name: 'Plateformes',
    component: Plateformes,
    meta: { value: 'Plateformes' },
    props: true,
  },
  {
    path: '/Profil',
    name: 'Profil',
    component: Profil,
    props: true,
  },
  {
    path: '/Commande/:idGame/:idOwner',
    name: 'Commande',
    component: Commande,
    props: true,
  },
  {
    path: '/Redirection',
    name: 'Redirection',
    component: Redirection,
    props: true,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
