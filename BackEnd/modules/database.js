require('dotenv').config();
const knexModule = require('knex');

const config = knexModule({
  client: 'mssql',
  connection: {
    server: 'localhost',
    user: process.env.DB_USER,
    password:  process.env.DB_PWD,
    database: process.env.DB_NAME,
    options: {
      trustedconnection: true,
      encrypt: true,
      enableArithAbort: true,
      trustServerCertificate: true
    },
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 30000
    },
  },
});

module.exports = config;