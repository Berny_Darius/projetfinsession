const jwt = require('jsonwebtoken');

module.exports = function authMiddleware(req, res, next) {
  try {
    const { headers } = req;
    const authHeader = headers.authorization;
    if (!authHeader) throw new Error('Header missing');
    if (!authHeader.startsWith('Bearer')) throw new Error('Bearer malformed');
    const secret = process.env.ACCESS_TOKEN_SECRET;
    const token = authHeader.slice(7);
    jwt.verify(token, secret);
    return next();
  } catch (error) {
    console.log(`Erreur s'est produite: ${error}`);
    return res.status(401).send('Not authorized');
  }
}