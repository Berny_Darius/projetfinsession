const express = require('express');
const bcrypt = require('bcrypt');
const Knex = require('../../modules/database');

const router = express.Router();

router.get('/:id', async(req, res) => {
  const { id } = req.params;
  try {
    let User = null;
    await Knex('Utilisateur')
    .where('Utilisateur.id_user', id)
    .then(result => {
      return User = result[0];
    })
    .catch(error => {
      res.status('400').json({
        message: 'Bad Request',
        error
      });
    })

    let History = null;
    await Knex('History')
    .select('History.id_history', 'History.id_game', 'Game.game_name', 'History.id_history_status', 'HistoryStatus.history_status')
    .leftJoin('Game', 'Game.id_game', 'History.id_game')
    .leftJoin('HistoryStatus', 'HistoryStatus.id_history_status', 'History.id_history_status')
    .where('History.id_user', id)
    .then(result => {
      return History = result;
    })
    .catch(error => {
      res.status('400').json({
        message: 'Bad Request',
        error
      });
    })

    /*let GameOwned = null;
    await Knex('Proprietaire')
    .select('Proprietaire.id_owner', 'Proprietaire.id_user', 'Proprietaire.id_game', 'Proprietaire.left_quantity', 'Game.game_name')
    .where('Proprietaire.id_user', id)
    .leftJoin('Game', 'Game.id_game', 'Proprietaire.id_game')
    .then(result => {
      return GameOwned = result;
    })
    .catch(error => {
      res.status('400').json({
        message: 'Bad Request',
        error
      });
    })*/

    res.status('200').json({
      message: 'success',
      User,
      History,
      //GameOwned
    });
  } catch (error) {
    console.log(error);
    res.status('400').json({
      message: 'Bad Request'
    });
  }
});

router.put('/:id', async (req, res) => {
  try{
    const { idUpdate, firstNameUpdate, lastNameUpdate, emailUpdate, passwordUpdate, adressUpdate, phoneNumberUpdate } = req.body;
    const { id } = req.params;

    let UserExist = await Knex('Utilisateur')
    .where('Utilisateur.id_user', id)
    .first();
    if (!UserExist) {
      return res.status(401).json({ message: 'Non authorized' });
    }

    let OneUser = await Knex('Utilisateur')
    .where('Utilisateur.email', emailUpdate)
    .first();
    if (OneUser && OneUser.id_user !== Number(idUpdate)) {
      return res.status(401).json({
        message: 'User already exists with that email'
      });
    }

    const HashedPassword = await bcrypt.hash(passwordUpdate, 8);
    await Knex('Utilisateur')
    .update({
      first_name: firstNameUpdate,
      last_name: lastNameUpdate,
      email: emailUpdate,
      password: HashedPassword,
      adress: adressUpdate,
      phone_number: phoneNumberUpdate,
    })
    .where('Utilisateur.id_user', id)
    .catch(error => {
      res.status(404).json({
        message: 'Error in the data, please try again',
        error
      });
    })
    .then(() => {
      return res.status(201).json({
        message: 'User updated successfuly',
        updated: true
      });
    })
  } catch (error) {
    res.status(400).json({
      message: 'Failed'
    })
  }
});

module.exports = router;