const express = require('express');
const Knex = require('../../modules/database');


const router = express.Router();
/*GET ALL the users */
router.get('/', async (req, res) => {
    try {
        let Utilisateur = null;
        await Knex('Utilisateur')
        .then(result => {
            return Utilisateur = result;
        })
        .catch(error => {
            res.status('400').json({
                message: 'Bad request',
                error
            });
        });
            
        res.status('200').json({
            message: 'success',
            Utilisateur
        });


    } catch (error) {
        console.log(error);
        res.status('400').json({ message: 'Bad request' });
    }
});

module.exports = router;
