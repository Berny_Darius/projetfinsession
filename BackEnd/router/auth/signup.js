const bcrypt = require('bcrypt');
const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();
/*POST user cred */
router.post('/', async (req, res) => {
  try {
    const { first_name, last_name, email, password, adress, phone_number } = req.body;
    const alreadyExist = await Knex('Utilisateur').where('email', email).first();

    if (alreadyExist) {
      return res.status(409).json({ message: 'User already exists', data: alreadyExist });
    } else {
      const hashedPassword = await bcrypt.hash(password, 8);
    await Knex('Utilisateur').insert({
      first_name: first_name,
      last_name: last_name,
      email: email,
      password: hashedPassword,
      adress: adress,
      phone_number: phone_number,
    }) .returning(['email', 'id_user'])
    .catch(error => {
      res.status(400).json({message: 'Error in the data, please try again', error});
    })
    .then(() => {
      return res.status(201).json({ message: 'User created successfully', created: true });
    });
    }
    
  } catch (error) {
     res.status(400).json({ message: 'failed' })
  }

});

module.exports = router;
