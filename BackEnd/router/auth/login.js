const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const knex = require('../../modules/database');
require('dotenv').config();

const router = express.Router();

router.post('/', async(req, res) => {
  try {
    const { email, password } = req.body;
    const user = await knex
    .select()
    .from('Utilisateur')
    .where('email', email)
    .first();

  if (!user) {
    return res.status(401).json({ message: 'Non authorized' });
  }

  const result = await bcrypt.compare(password, user.password);
  
  if (!result) {
    return res.status(401).json({ message: 'Non authorized '});
  }

  const payload = {
    userID: user.id_user
  };
  const token = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET);
  
  return res.status(200).json({ token });
} 
  catch (err) {
    console.log(err);
    res.status(403).json({ message: 'failed' })
}
  
});

router.post('/logout', async (req, res) => {
  delete process.env.ACCESS_TOKEN_SECRET;
  res.status(200).json({ message: 'Disconnected' });
});
module.exports = router;