const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();
/*GET the games from a genre */
router.get('/:id', async (req,res) => {
  try {
    const { id } = req.params;
    let Games = null;
    await Knex('Game')
    .select('Game.id_game','Game.game_name','Game.game_description','Game.game_price','Game.img_game','Game.id_developper','Game.id_game_status','Game.id_genre','Game.id_platforms', 'Genre.genre_name')
    .join('Genre', 'Genre.id_genre', 'Game.id_genre')
    .where('Game.id_genre', id)
    .orderBy('game_name')
    .then(result => {
      return Games = result;
    }).catch(error => {
      res.status('400').json({
        message: 'Bad request',
        error
      });
    });

    let Images = null;
    await Knex('Images')
    .select('Images.id_img','Images.imgURL','Images.id_game','Images.id_dev','Images.id_platforms','Game.id_genre')
    .join('Game', 'Game.id_game', 'Images.id_game')
    .where('Game.id_genre', id)
    .then(result => {
      return Images = result;
    }).catch(e => {
      res.status('400').json({ message: 'Bad request', e });
    });

    res.status('200').json({
      message: 'success',
      Games,
      Images
    });
  } catch (error) {
    console.log(error);
    res.status('400').json({ message: 'Bad request' });
  }
});

module.exports = router;
