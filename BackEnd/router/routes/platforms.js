const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();
/* GET THE PLATFORMS OF GAMES */
router.get('/', async (req, res) => {
  try {
    let Platforms = null;
        
        await Knex('Platforms')
        .orderBy('platforms_name')
        .then(result => {
            return Platforms = result;
        })
        .catch(error => {
            res.status('400').json({
                message: 'Bad request',
                error
            });
        });
        let Images = null; 
        await Knex('Images')
        .then(result => {
            return Images = result;
        }).catch(error => {
            res.status('400').json({
                message: 'Bad request',
                error
            });
        });
        res.status('200').json({
            message: 'success',
            Platforms,
            Images
        });
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;
