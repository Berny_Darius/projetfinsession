const express = require('express');
const Knex = require('../../modules/database');
const fetch = require('node-fetch')

const router = express.Router();

/*GET ALL the games */
router.get('/', async (req, res) => {
    try {
        let Games = null;
        let Images = null;
        await Knex('Game')
            .orderBy('game_name')
            .then(result => {
                return Games = result;
            }).catch(error => {
                res.status('400').json({
                    message: 'Bad request',
                    error
                });
            });


        await Knex('Images')
            .then(result => {
                return Images = result;
            }).catch(error => {
                res.status('400').json({
                    message: 'Bad request',
                    error
                });
            });
        res.status('200').json({
            message: 'success',
            Games,
            Images
        });
    } catch (error) {
        console.log(error);
        res.status('400').json({ message: 'Bad request' });
    }
});

router.get('/:Recherche/:Genre', async (req, res) => {
    try {
        const { Recherche } = req.params ? req.params : null
        const { Genre } = req.params;
        let Games = await Knex('Game').orderBy('game_name');
        let Images = await Knex('Images').orderBy('imgURL');

        if (Recherche !== 'null') {
            if (Genre === 'Genres') {
                Games = Object.values(Games).filter(game => { return game.game_name.toUpperCase().indexOf(Recherche.toUpperCase()) !== -1 });
            Images = Object.values(Games).map((game) => {
                return Object.values(Images).find((image) => image.id_game === game.id_game)
            })
            } else {
                Games = Object.values(Games)
                .filter(game => {
                    if (game.id_genre === parseInt(Genre)) {
                        return game.game_name.toUpperCase().indexOf(Recherche.toUpperCase()) !== -1;
                    }
                });
                Images = Object.values(Games).map((game) => {
                    return Object.values(Images).find((image) => image.id_game === game.id_game)
                });
            }
        } else if (Recherche === 'null') {
            if (Genre === 'Genres') {
                Games = await Knex('Game').orderBy('game_name');
            Images = await Knex('Images').orderBy('imgURL');
            } else {
                await Knex('Game')
                .select('Game.id_game','Game.game_name','Game.game_description','Game.game_price','Game.img_game','Game.id_developper','Game.id_game_status','Game.id_genre','Game.id_platforms', 'Genre.genre_name')
                .join('Genre', 'Genre.id_genre', 'Game.id_genre')
                .andWhere('Game.id_genre', Genre)
                .orderBy('game_name')
                .then(result => {
                    return Games = result;
                }).catch(error => {
                    res.status('400').json({
                        message: 'Bad request',
                        error
                    });
                });
        
                await Knex('Images')
                .select('Images.id_img','Images.imgURL','Images.id_game','Images.id_dev','Images.id_platforms','Game.id_genre')
                .join('Game', 'Game.id_game', 'Images.id_game')
                .where('Game.id_genre', Genre)
                .then(result => {
                    return Images = result;
                })
                .catch(e => {
                    res.status('400').json({ message: 'Bad request', e });
                });
            }
        }

        res.status('200').json({
            message: 'Success!',
            Games,
            Images,
        });
    } catch (error) {
        console.log(error);
        res.status('400').json({ message: 'Bad request', error: error });
    }
});

module.exports = router;