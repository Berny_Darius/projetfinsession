const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();
/*GET ALL the details from a id_game */
router.get('/:id', async (req, res) => {
  const { id } = req.params;
  try {
    let Game = null;
    await Knex('Game')
    .select('Game.id_game','Game.game_name','Game.game_description','Game.game_price','Game.img_game','Game.id_developper','Game.id_game_status','Game.id_genre','Game.id_platforms', 'Genre.genre_name','Developper.developper_name','GameStatus.game_status','Platforms.platforms_name')
    .leftJoin('Developper', 'Developper.id_developper', 'Game.id_developper')
    .leftJoin('GameStatus', 'GameStatus.id_game_status', 'Game.id_game_status')
    .leftJoin('Genre', 'Genre.id_genre', 'Game.id_genre')
    .leftJoin('Platforms', 'Platforms.id_platforms', 'Game.id_platforms')
    .where('Game.id_game', id)
    .orderBy('game_name')
    .then(result => {
      return Game = result[0];
    })
    .catch(error => {
      res.status('400').json({
        message: 'Bad request',
        error
      });
    });

    let Proprietaire = null;
    await Knex('Proprietaire')
    .where('id_game', id)
    .leftJoin('Utilisateur', 'Utilisateur.id_user', 'Proprietaire.id_user')
    .then(result => {
       return Proprietaire = result;
    })
    .catch(error => {
      res.status('400').json({
        message: 'Bad request',
         error
      });
    });

    let Images = null;
    await Knex('Images')
    .where('id_game', id)
    .then(result => {
      return Images = result[0];
    }).catch(error => {
      res.status('400').json({
        message: 'Bad request',
        error
      });
    });
    res.status('200').json({
      message: 'success',
      Game,
      Images,
      Proprietaire
    });
  } catch (error) {
    console.log(error);
    res.status('400').json({ message: 'Bad request' })
  }
});

router.post('/:id', async (req, res) => {
  const { id } = req.params;
  try {
    const { left_quantity, due_date, id_user } = req.body;
    await Knex('Proprietaire').insert({
      left_quantity: left_quantity,
      due_date: due_date,
      id_user: id_user,
      id_game: id,
    }) .catch(error => {
      res.status(400).json({
        message: 'Error in the Data',
        error
      });
    }) .then(() => {
      return res.status(201).json({
        message: 'Owner created successfully',
        created: true
      });
    });
  } catch (error) {
    res.status(400).json({
      message: 'Failed'
    });
  }
});

module.exports = router;

