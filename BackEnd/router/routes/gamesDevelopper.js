const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();
/*GET the games from a Platform */
router.get('/:id', async (req,res) => {
  try {
    const { id } = req.params;
    let Games = null; 
    await Knex('Game')
    .select('Game.id_game','Game.game_name','Game.game_description','Game.game_price','Game.img_game','Game.id_developper','Game.id_game_status','Game.id_genre','Game.id_platforms', 'Developper.developper_name')
    .join('Developper','Game.id_developper', 'Developper.id_developper')
    .where('Game.id_developper', id)
    .orderBy('game_name')
    .then(result => {
      return Games = result;
    }).catch(error => {
      res.status('400').json({
        message: 'Bad request',
        error
      });
    });

    let Images = null;
    await Knex('Images')
    .where('id_dev', id)
    .then(result => {
      return Images = result;
    }).catch(error => {
      res.status('400').json({
        message: 'Bad request',
        error
      });
    });

    res.status('200').json({
      message: 'success',
      Games,
      Images
    });
  } catch (error) {
    onsole.log(error);
    res.status('400').json({ message: 'Bad request' });
  }
});

module.exports = router;
