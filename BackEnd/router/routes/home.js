const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();
/*GET ALL the games who are disponible */
router.get('/', async (req, res) => {
    try {
        let Games = null;
        let Images = null;

        await Knex('Game')
                .select('Game.id_game','Game.game_name','Game.game_description','Game.game_price','Game.img_game','Game.id_developper','Game.id_game_status','Game.id_genre','Game.id_platforms', 'Proprietaire.id_owner', 'Proprietaire.left_quantity','Utilisateur.first_name', 'Utilisateur.last_name','Utilisateur.email','Utilisateur.phone_number')
                .join('Proprietaire', 'Proprietaire.id_game', 'Game.id_game')
                .join('Utilisateur', 'Utilisateur.id_user', 'Proprietaire.id_user')
                .orderBy('game_name')
                .then(result => {
                    return Games = result;
                }).catch(error => {
                    res.status('400').json({
                        message: 'Bad request',
                        error
                    });
                });
        
                await Knex('Images')
                .select('Images.id_img','Images.imgURL','Images.id_game','Images.id_dev','Images.id_platforms','Game.id_genre')
                .join('Game', 'Game.id_game', 'Images.id_game')
                .then(result => {
                    return Images = result;
                })
                .catch(e => {
                    res.status('400').json({ message: 'Bad request', e });
                });
        res.status('200').json({
            message: 'success',
            Games,
            Images
        });
    } catch (error) {
        console.log(error);
        res.status('400').json({
            message: error.originalError,
            error
        });
    }
});

module.exports = router;
