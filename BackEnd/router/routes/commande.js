const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();

router.get('/:idGame/:idOwner', async (req, res) => {
  const { idGame } = req.params;
  const { idOwner } = req.params;
  try {
    let Game = null;
    await Knex('Game')
    .where('id_game', idGame)
    .then(result => {
      return Game = result[0];
    }).catch(error => {
      res.status('400').json({
        message: 'Bad Request',
        error
      });
    });

    let Proprietaire = null;
    await Knex('Proprietaire')
    .select('Proprietaire.id_owner', 'Proprietaire.left_quantity', 'Proprietaire.due_date', 'Proprietaire.id_user', 'Utilisateur.first_name', 'Utilisateur.last_name')
    .leftJoin('Utilisateur', 'Utilisateur.id_user', 'Proprietaire.id_user')
    .where('Proprietaire.id_owner', idOwner)
    .then(result => {
      return Proprietaire = result[0];
    }).catch(error => {
      res.status('400').json({
        message: 'Bad Request',
        error
      });
    });

    let Images = null;
    await Knex('Images')
    .where('Images.id_game', idGame)
    .then(result => {
      return Images = result[0];
    }).catch(error => {
      res.status('400').json({
        message: 'Bad Request',
        error
      });
    });

    res.status('200').json({
      message: 'Success',
      Game,
      Proprietaire,
      Images
    });
  } catch (error){
    console.log(error);
    res.status('400').json({
      message: 'Bad Request',
    });
  }
});

router.post('/:idGame/:idOwner', async(req, res) => {
  const { idGame } = req.params;
  const { idOwner } = req.params;
  try{
    const { no_commande, id_user } = req.body;
    await Knex('Commande').insert({
      no_commande: no_commande,
      id_user: id_user,
      id_game: idGame,
      id_owner: idOwner,
    }).catch(error => {
      res.status(400).json({
        message: 'Error in the Data',
        error
      });
    }).then((result) => {
      return res.status(201).json({
        message: 'Command created successfully',
        result,
      });
    });

    await Knex('History').insert({
      id_user: id_user,
      id_game: idGame,
      id_history_status: 2, // 2 = non remis
    }).catch(error => {
      res.status(400).json({
        message: 'Error in the Data',
        error
      });
    }).then((result) => {
      return res.status(201).json({
        message: 'History created successfully',
        result,
      });
    });
  } catch (error) {
    res.status(400).json({
      message: 'Failed',
    });
  }
});

module.exports = router;
