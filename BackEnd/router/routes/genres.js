const express = require('express');
const Knex = require('../../modules/database');


const router = express.Router();
/* GET THE GENRES OF GAMES */
router.get('/', async (req, res) => {
    try {
        let Genres = null;
        await Knex('Genre')
        .orderBy('genre_name')
        .then(result => {
            return Genres = result;
        })
        .catch(error => {
            res.status('400').json({
                message: 'Bad request',
                error
            });
        });
        res.status('200').json({
            message: 'success',
            Genres
        });
    } catch (error) {
        console.log(error);
        res.status('400').json({ message: 'Bad request' });
    }
});

module.exports = router;
