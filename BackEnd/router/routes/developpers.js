const express = require('express');
const Knex = require('../../modules/database');

const router = express.Router();
/*GET ALL the developpers who made a game */
router.get('/', async (req, res) => {
  try {
    let Developpers = null;
    await Knex('Developper')
    .orderBy('developper_name')
    .then(result => {
      return Developpers = result;
    })
    .catch(error => {
      res.status('400').json({
        message: 'Bad request',
        error
      });
    });
    let Images = null;
    await Knex('Images')
    .leftJoin('Developper', 'Developper.id_developper', 'Images.id_dev')
    .then(result => {
      return Images = result;
    }).catch(error => {
      res.status('400').json({
        message: 'Bad request',
        error
      });
    });
    res.status('200').json({
      message: 'success',
      Developpers,
      Images
    });
  } catch (error) {
    console.log(error);
    res.status('400').json({ message: 'Bad request' })
  }
});

module.exports = router;

