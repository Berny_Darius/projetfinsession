const { Router } = require('express');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { route } = require('./router/routes/developpers');
const req = require('express/lib/request');
const authMiddleware = require('./modules/authMiddleware');


const router = express.Router();

const app = express();
const routes = './router/routes';
const port = 3000;
// Require routes
const developpers = require(`${routes}/developpers`);
const home = require(`${routes}/home`);
const genres = require(`${routes}/genres`);
const platforms = require(`${routes}/platforms`);
const gamesGenre = require(`${routes}/gamesGenre`);
const gamesPlatform = require(`${routes}/gamesPlatform`);
const gamesList = require(`${routes}/gamesList`);
const gameDetails = require(`${routes}/gameDetails`);
const gamesDevelopper = require(`${routes}/gamesDevelopper`);
const commande = require(`${routes}/commande`);
const profil = require(`./router/users/profil`);
const users = require('./router/users/users');
const signup = require(`./router/auth/signup`);
const login = require('./router/auth/login');
app.use(express.json())
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '200mb' }));

app.use(express.static('uploads'));


// Use router
app.use('/api', router);
app.use('/api/home', home);
app.use('/api/developpers', developpers);
app.use('/api/genres', genres);
app.use('/api/platforms', platforms);
app.use('/api/signup', signup);
app.use('/api/gamesGenre', gamesGenre)
app.use('/api/gamesPlatform', gamesPlatform)
app.use('/api/gamesList', gamesList)
app.use('/api/users', users)
app.use('/api/gameDetails',gameDetails)
app.use('/api/gamesDevelopper', gamesDevelopper)
app.use('/api/commande', commande);
app.use('/api/profil', profil);
app.use('/api/token',login);



app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});