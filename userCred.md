USER CRED
{
	"id_user": 1,
	"first_name": "John",
	"last_name": "Doe",
	"email": "JohnDoe@gmail.com",
	"password": "IamABot3000", <== this is the password before it is hashed
	"hashedPassword": "$2b$08$NlpUmYCktf8JHj.6rDwAh.ZhvanDxqOGGad7gzErhSz3upHXerrTy",
	"adress": "3248 Diamond Cove,Providence;Rhode Island,02905",
	"phone_number": "401-499-5747"
},
{
	"id_user": 2,
	"first_name": "Carl",
	"last_name": "Nagel",
	"email": "elmo1984@yahoo.com",
	"password": "elmo1984", <== this is the password before it is hashed
	"hashedPassword": "$2b$08$QeQkFczp9/7.dbBJcM.qu.AWJudEKpF72pUjPCQ8I/.iBZUONjjDu",
	"adress": "1300 Florence Street,Sulphur Springs;Texas,75482",
	"phone_number": "903-335-4754"
},
{
	"id_user": 3,
	"first_name": "David ",
	"last_name": "Meldrum",
	"email": "deion1973@yahoo.com",
	"password": "deion1973", <== this is the password before it is hashed
	"hashedPassword": "$2b$08$VWQo9AnXDFzCJViIG8wUS.5lqrRmhWfkPb2z5sQ8I8MMvBCGK2gU6",
	"adress": "614 Johnson Street,Cary;Washington,98168",
	"phone_number": "425-326-8873"
},
{
	"id_user": 4,
	"first_name": "Sally",
	"last_name": "McGuire",
	"email": "genesis1998@yahoo.com",
	"password": "genesis1998", <== this is the password before it is hashed
	"hashedPassword": "$2b$08$fHU5yLT0XHCvRSG9cIiGDuswEnsfvb6ChaHh0PhkUY.EJLmV.pohW",
	"adress": "3248 Diamond Cove,Providence;North Carolina,02905",
	"phone_number": "401-499-5747"
},
{
	"id_user": 5,
	"first_name": "Paul",
	"last_name": "Walker",
	"email": "paulWalker@yahoo.com",
	"password": "paulWalker01", <== this is the password before it is hashed
	"hashedPassword": "$2b$08$Jzzykdl8wcUVHDdIqL7lo.QERz5FbU5XA/gx9F1pdjbGZVMqrHPFq",
	"adress": "12 rue des pionniers, h1z 90q westmount,Montreal",
	"phone_number": "321 654 7789"
}
	###FOR TESTING PURPOSES
{
	"email": "JohnDoe@gmail.com",
	"password": "IamABot3000"
}
{
	"email": "elmo1984@yahoo.com",
	"password": "elmo1984"
}
{
	"email": "deion1973@yahoo.com",
	"password": "deion1973"
}
{
	"email": "genesis1998@yahoo.com",
	"password": "genesis1998"
}
	##THESE DON'T EXIST, I CREATED THEM
{
	"email": "paulWalker@yahoo.com",
	"password": "paulWalker01"
}
{
	"email": "PatVlad@gmail.com",
	"password": "RussiaIsTheBest07"
}
{
	"email": "Horseman@gmail.com",
	"password": "jackyHo005"
}
{
	"email": "MisterObama@gmail.com",
	"password": "yesWeCan51"
}