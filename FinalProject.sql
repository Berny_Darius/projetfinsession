--Cr�ation de la base de donn�es--
use master
go

if db_id ('FinalProject') is not null
begin

--Terminer toutes les connections � la base de donn�es--
	use master;
	declare @kill varchar(8000) = '';
	select @kill = @kill + 'kill' + convert(varchar(5), session_id) + ';'
	from sys.dm_exec_sessions
	where database_id = db_id('FinalProject');
	exec(@kill);

--Effacer la base de donn�es--
	drop database FinalProject;

--Cr�er la base de donn�es--
	create database FinalProject;
end
else
begin

--Cr�er la base de donn�es--
	create database FinalProject;
	end
go

--Utilisation de la base de donn�es--
use FinalProject
go

--Cr�ation de la Table Utilisateur--
create table Utilisateur(
	id_user int identity(1,1) not null, --Champ Identity pour Primary Key--
	first_name nvarchar(MAX) not null,
	last_name nvarchar(MAX) not null,
	email nvarchar(MAX) not null,
	password nvarchar(MAX) not null,
	adress nvarchar(MAX) not null,
	phone_number nvarchar(MAX) not null,

	--Ajout d'une Primary Key--
	constraint pk_id_user primary key(id_user)
)
go

--Cr�ation de la table GameStatus--
create table GameStatus(
	id_game_status int identity(1,1) not null, --Champ Identity pour Primary Key--
	game_status varchar(50)      --Could be "Disponible" or "indisponible"

	--Ajout d'une Primary Key--
	constraint pk_GameStatus primary key(id_game_status)
)
go

--Cr�ation de la table Genre--
create table Genre(
	id_genre int identity(1,1) not null, --Champ Identity pour Primary Key--
	genre_name varchar(50) not null,
	--Ajout d'une Primary Key--
	constraint pk_id_genre primary key (id_genre),
)
go

--Cr�ation de la table Platforms--
create table Platforms(
	id_platforms int identity(1,1) not null, --Champ Identity pour Primary Key--
	platforms_name varchar(50) not null,
	--Ajout d'une Primary Key--
	constraint pk_id_platforms primary key (id_platforms),
)
go

--Cr�ation de la table Developper--
create table Developper(
	id_developper int identity(1,1) not null, --Champ Identity pour Primary Key--
	developper_name varchar(50) not null,
	--Ajout d'une Primary Key--
	constraint pk_id_developper primary key (id_developper),
)
go

--Cr�ation de la table HistoryStatus--
create table HistoryStatus(
	id_history_status int identity(1,1) not null, --Champ Identity pour Primary Key--
	history_status varchar(50) not null,   --Remis, � remettre ou en retard

	--Ajout d'une Primary Key--
	constraint pk_HistoryStatus primary key (id_history_status),
)
go

--Cr�ation de la table Game--
create table Game(
	id_game int identity(1,1) not null, --Champ Identity pour Primary Key--
	game_name varchar(50) not null,
	game_description nvarchar(MAX),
	game_price float not null,
	img_game nvarchar(max) not null,
	id_developper int not null,
	id_game_status int not null,
	id_genre int NOT NULL,
	id_platforms int NOT NULL,

	--Ajout d'une Foreign Key--
	constraint fk_Game_id_game_status foreign key (id_game_status) references GameStatus (id_game_status),
	constraint fk_Game_id_genre foreign key (id_genre) references Genre (id_genre),
	constraint fk_Game_id_platforms foreign key (id_platforms) references Platforms (id_platforms),
	constraint fk_Game_id_developper foreign key (id_developper) references Developper (id_developper),


	--Ajout d'une Primary Key--
	constraint pk_id_game primary key (id_game),
)
go

--Cr�ation de la table Images 
Create TABLE Images( 
	 id_img int  PRIMARY KEY NOT NULL Identity(1,1), 
	 imgURL NVARCHAR(MAX)not null,
	 id_game INT,
	 id_dev int,
	 id_platforms int

	--Ajout des Foreign Key--
	CONSTRAINT fk_Image_id_game FOREIGN KEY (id_game) REFERENCES Game(id_game),
	CONSTRAINT fk_Image_id_dev FOREIGN KEY (id_dev) REFERENCES Developper(id_developper),
	CONSTRAINT fk_Image_id_platforms FOREIGN KEY (id_platforms) REFERENCES Platforms (id_platforms)
)
GO

--Cr�ation de la table Propri�taire--
create table Proprietaire(
	id_owner int identity(1,1) not null, --Champ Identity pour Primary Key--
	left_quantity int not null,
	due_date date not null, --YY/MM/DD--
	id_user int not null,
	id_game int not null,

	--Ajout des Foreign Key--
	constraint fk_Owner_User foreign key (id_user) references Utilisateur (id_user),
	constraint fk_Owner_Game foreign key (id_game) references Game (id_game),

	--Ajout d'une Primary Key--
	constraint pk_Owner primary key (id_owner),
)
go

--Cr�ation de la table Commande--
create table Commande(
	id_commande int identity(1,1) not null, --Champ Identity pour Primary Key--
	no_commande varchar(50),
	id_user int not null,
	id_game int not null,
	id_owner int not null,

	--Ajout des Foreign Key--
	constraint fk_Commande_User foreign key (id_user) references Utilisateur (id_user),
	constraint fk_Commande_Game foreign key (id_game) references Game(id_game),
	constraint fk_Commande_Owner foreign key (id_owner) references Proprietaire(id_owner),

	--Ajout d'une Primary Key--
	constraint pk_Commande primary key (id_commande),
)
go

--Cr�ation de la table History--
create table History(
	id_history int identity(1,1) not null, --Champ Identity pour Primary Key--
	id_user int not null,
	id_game int not null,
	id_history_status int not null,

	--Ajout des Foreign Key--
	constraint fk_History_User foreign key (id_user) references Utilisateur (id_user),
	constraint fk_History_Game foreign key (id_game) references Game (id_game),
	constraint fk_History_HistoryStatus foreign key (id_history_status) references HistoryStatus (id_history_status),

	--Ajout d'une Primary Key--
	constraint pk_History primary key (id_history),
)
go

--Insertion des utilisateurs--
INSERT INTO Utilisateur VALUES
('John','Doe','JohnDoe@gmail.com','$2b$08$NlpUmYCktf8JHj.6rDwAh.ZhvanDxqOGGad7gzErhSz3upHXerrTy','3248 Diamond Cove,Providence;Rhode Island,02905',
'401-499-5747'),
('Carl','Nagel','elmo1984@yahoo.com','$2b$08$QeQkFczp9/7.dbBJcM.qu.AWJudEKpF72pUjPCQ8I/.iBZUONjjDu','1300 Florence Street,Sulphur Springs;Texas,75482',
'903-335-4754'),
('David ','Meldrum','deion1973@yahoo.com','$2b$08$VWQo9AnXDFzCJViIG8wUS.5lqrRmhWfkPb2z5sQ8I8MMvBCGK2gU6','614 Johnson Street,Cary;Washington,98168',
'425-326-8873'),
('Sally','McGuire','genesis1998@yahoo.com','$2b$08$fHU5yLT0XHCvRSG9cIiGDuswEnsfvb6ChaHh0PhkUY.EJLmV.pohW','3248 Diamond Cove,Providence;North Carolina,02905',
'401-499-5747')
go

--Insertion des genres de jeux--
INSERT INTO Genre VALUES
('Sandbox'),
('Real-time strategy (RTS)'),
('Shooters (FPS)'),
('Role-playing (RPG)'),
('Sports'),
('Puzzles'),
('Card game'),
('Simulation '),
('Action/Adventure '),
('Indie'),
('Party')
GO

--Insertion des platformes de jeux--
INSERT INTO Platforms VALUES
('PS4'),
('PS5'),
('Xbox Series X'),
('Xbox Series S'),
('Xbox One S'),
('Xbox One X'),
('PC'),
('Nintendo Switch')
GO

--Insertion des status de jeux--
INSERT INTO GameStatus VALUES
('Disponible'),
('Indisponible')
GO

--Insertion des d�veloppeurs de jeux--
INSERT INTO Developper VALUES
('Ubisoft'),
('Bethesda'),
('Sucker Punch'),
('Naughty Dog'),
('Nintendo'),
('Guerilla Games'),
('Activision'),
('Santa Monica Studio'),
('Rockstar Games'),
('EA SPORTS'),
('EA Originals'),
('2K Games')
GO

--Insertion des status d'historiques--
INSERT INTO HistoryStatus VALUES 
('Remis'),
('Non-remis'),
('En retard')
GO

--Insertion des jeux--
INSERT INTO Game VALUES
('NBA2K22','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
29.99,'NBA2K22 PS4',12,1,5,1),
('God of War','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
29.99,'God of War PC',8,2,9,7),
('FIFA22','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
29.99,'FIFA22 PS5',10,1,5,2),
('Assassin''s Creed: Valhalla', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
59.99,'Assassin''s Creed Valhalla PS4',1,2,9,1),
('Fallout 76', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
54.99,'Fallout 76 Xbox Series X',2,2,3,3),
('inFAMOUS Second Son','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
19.99,'inFAMOUS Second Son PS4',3,2,9,1),
('The Last of Us Part II', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
53.49,'The Last of Us Part II PS5',4,2,3,2),
('The Legend Of Zelda: Breath Of The Wild', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
59.99,'The Legend Of Zelda Breath Of The Wild Nintendo Switch',5,2,9,8),
('Horizon Zero Dawn', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
29.99,'Horizon Zero Dawn PS4',6,2,9,1),
('Call of Duty: Vanguard', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
79.99,'Call of Duty Vanguard Xbox Series S',7,2,3,4),
('Grand Theft Auto 5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
19.99,'Grand Theft Auto 5 Xbox One X',9,2,9,6),
('Mario Kart 8 Deluxe', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
79.99,'Mario Kart 8 Deluxe Nintendo Switch',5,2,5,8),
('Far Cry 6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet luctus venenatis lectus magna fringilla. Sagittis orci a scelerisque purus semper eget duis. Id volutpat lacus laoreet non curabitur. In fermentum et sollicitudin ac orci phasellus egestas tellus. Orci porta non pulvinar neque. Sapien eget mi proin sed libero enim. Nibh venenatis cras sed felis eget velit aliquet. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Condimentum lacinia quis vel eros. Lacus sed viverra tellus in hac habitasse platea dictumst. In hendrerit gravida rutrum quisque non tellus orci ac auctor. Elementum eu facilisis sed odio.
Eu mi bibendum neque egestas congue quisque egestas diam in. Tellus integer feugiat scelerisque varius morbi enim. Sit amet risus nullam eget. Ut pharetra sit amet aliquam id. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus. Nibh venenatis cras sed felis eget velit. Facilisis sed odio morbi quis commodo. Cursus sit amet dictum sit amet justo donec enim. Cursus turpis massa tincidunt dui ut ornare lectus sit. Ac odio tempor orci dapibus ultrices in iaculis nunc sed.',
58.99,'Far Cry 6 Xbox One S',1,2,3,5)
GO

INSERT INTO Images VALUES 
('NBA2K22 PS4', 1, 12, 1),
('God of War PC', 2, 8, 7),
('FIFA22 PS5', 3, 10, 1),
('Assassin''s Creed Valhalla PS4', 4, 1, 1),
('Fallout 76 Xbox Series X', 5, 2, 3),
('inFAMOUS Second Son PS4', 6, 3, 1),
('The Last of Us Part II PS5', 7, 4, 1),
('The Legend Of Zelda Breath Of The Wild Nintendo Switch', 8, 5, 8),
('Horizon Zero Dawn PS4', 9, 6, 1),
('Call of Duty Vanguard Xbox Series S', 10, 7, 3),
('Grand Theft Auto 5 Xbox One X', 11, 9, 3),
('Mario Kart 8 Deluxe Nintendo Switch', 12, 5, 8),
('Far Cry 6 Xbox One S', 13, 1, 3),
('Ubisoft', null, 1, null),
('Bethesda', null, 2, null),
('Sucker Punch', null, 3, null),
('Naughty Dog', null, 4, null),
('Nintendo', null, 5, null),
('Guerilla Games', null, 6, null),
('Activision', null, 7, null),
('Santa Monica Studio', null, 8, null),
('Rockstar Games', null, 9, null),
('EA SPORTS', null, 10, null),
('EA Originals', null, 11, null),
('2K Games', null, 12, null),
('PS4', null, null,1),
('PS5', null, null,1),
('Xbox Series X', null, null,3),
('Xbox Series S', null, null,3),
('Xbox One S', null, null,3),
('Xbox One X', null, null,3),
('PC', null, null,7),
('Nintendo Switch', null, null,8)
GO

--Insertion des propri�taires--
INSERT INTO Proprietaire VALUES
(4,'2022-02-05',2,1)
GO

--Insertion des commandes--
INSERT INTO Commande VALUES
('000',1,1,1)
GO

--Insertion des historiques des utilisateurs--
INSERT INTO History VALUES
(2,2,1)
GO

create login FinalProjectLogin with password='abc123'
go
USE FinalProject;
Go
create user fpLogin from login FinalProjectLogin
go
GRANT insert,update,delete,select ON SCHEMA :: [dbo] TO fpLogin
go

select * 
from Utilisateur
GO


select * 
from Genre
go

select * 
from Images
go

select * 
from Game
go

select * 
from Platforms
go

select * 
from GameStatus
go

select * 
from Developper
go

select * 
from HistoryStatus
go


select * 
from Commande c
go

select * 
from History
go

select * 
from Proprietaire
go